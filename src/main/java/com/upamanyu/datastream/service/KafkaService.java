package com.upamanyu.datastream.service;

import java.io.IOException;

public interface KafkaService {

    void generateAndSendToKafka();

    void readCoordinatesAndSendToKafka(String topic) throws IOException;

}
