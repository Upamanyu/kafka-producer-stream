package com.upamanyu.datastream.service.impl;

import com.upamanyu.datastream.DTO.LocationDTO;
import com.upamanyu.datastream.constants.TruckNames;
import com.upamanyu.datastream.service.KafkaService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.xml.stream.Location;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.Iterator;

@Service
public class KafkaServiceImpl implements KafkaService {

    @Value("${kafka.producer.topic}")
    private String topic;

    @Autowired
    private KafkaTemplate<String, LocationDTO> kafkaTemplate;

    private SecureRandom secureRandom = new SecureRandom();

    @Override
    public void generateAndSendToKafka() {
        // generate a random location and send
        int index = secureRandom.nextInt(0,TruckNames.NAMES.length);

        double latitude = secureRandom.nextDouble(20.0,30.0);
        double longitude = secureRandom.nextDouble(80.0,83.0);
        BigDecimal bdLat = new BigDecimal(latitude);
        BigDecimal bdLon = new BigDecimal(longitude);
        bdLat.setScale(2, RoundingMode.HALF_UP);
        bdLon.setScale(2, RoundingMode.HALF_UP);
        latitude = bdLat.doubleValue();
        longitude = bdLon.doubleValue();
        String name = TruckNames.NAMES[index]; ;
        LocationDTO location = new LocationDTO(latitude,longitude);
        kafkaTemplate.send(topic,"KEY",location);
    }

    @Override
    public void readCoordinatesAndSendToKafka(String topic) throws IOException {
        InputStream is = new ClassPathResource("files/kol_to_chen 1.xlsx")
                .getInputStream();
        Workbook workbook = new XSSFWorkbook(is);
        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rows = sheet.iterator();

        int rowIndex = 1;

        if (rows.hasNext()) {
            rows.next();
        }

        while(rows.hasNext()) {
           Row currentRow = rows.next();
           if(rowIndex%4==0) {
               // get every 4th row
               Cell latitudeCell = currentRow.getCell(0);
               Cell longitudeCell = currentRow.getCell(1);
               LocationDTO locationDTO = new LocationDTO();
               if(latitudeCell !=null) {
                   locationDTO.setLatitude(latitudeCell.getNumericCellValue());
               }
               if(longitudeCell != null) {
                   locationDTO.setLongitude(longitudeCell.getNumericCellValue());
               }
               kafkaTemplate.send(topic,locationDTO);
               try {
                   Thread.sleep(2000);
               } catch (InterruptedException e) {
                   Thread.currentThread().interrupt();
                   throw new RuntimeException("Thread interrupted", e);
               }
           }
           rowIndex++;
        }
        workbook.close();
        is.close();
    }

}
