package com.upamanyu.datastream.serializer;

import com.upamanyu.datastream.DTO.LocationDTO;
import org.apache.kafka.common.serialization.Serializer;

import java.nio.charset.StandardCharsets;

public class LocationSerializer implements Serializer<LocationDTO> {


    @Override
    public byte[] serialize(String topic, LocationDTO data) {
        String name = "12345";
        double latitude = data.getLatitude();
        double longitude = data.getLongitude();
        String serializedData = name+"|"+latitude+"|"+longitude;
        System.out.println(serializedData);
        return serializedData.getBytes(StandardCharsets.UTF_8);
    }

}
