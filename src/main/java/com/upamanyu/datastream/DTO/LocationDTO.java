package com.upamanyu.datastream.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationDTO implements Serializable {

    private double latitude;
    private double longitude;

}
