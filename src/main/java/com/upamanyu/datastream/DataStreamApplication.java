package com.upamanyu.datastream;

import com.upamanyu.datastream.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

@SpringBootApplication
public class DataStreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataStreamApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(@Autowired KafkaService kafkaServiceImpl) {
		return runner -> {
				//generateRandomDataStream(kafkaServiceImpl);
				String topic = "location-topic";
				generateCoordinateStream(kafkaServiceImpl,topic);
				//System.out.println("Current working directory: " + System.getProperty("user.dir"));
		};
	}

	private static void generateRandomDataStream(KafkaService kafkaServiceImpl) throws InterruptedException {
		// every 2 seconds send coordinates
		while(true) {
			kafkaServiceImpl.generateAndSendToKafka();
			Thread.sleep(2000);
		}
	}

	private static void generateCoordinateStream(KafkaService kafkaServiceImpl,String topic)  {
		try {
			kafkaServiceImpl.readCoordinatesAndSendToKafka(topic);
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
